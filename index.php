<?php declare(strict_types=1);

use Task\TaskManager;

require 'vendor/autoload.php';

if(!isset($argv[1]) || $argv[1] == "help"){
   echo "Mozliwe argumenty: \n";
   foreach(TaskManager::TASKS as $task){
       echo $task."\n";
   }
   return;
}

$launcher = new TaskManager();
$launcher->launchTask($argv[1]);