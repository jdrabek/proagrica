<?php

namespace Test;

use PHPUnit\Framework\TestCase;
use Task\ITask;
use Task\TaskManager;

class TaskTest extends TestCase
{

    private $launcher;
    public function setUp() : void
    {
        $this->launcher = new TaskManager();

    }
    public function testGetTaskList()
    {
        $taskList = $this->launcher->getTasks();
        $this->assertIsIterable($taskList);
        $this->assertCount(1,$taskList);

}
    public function testLaunchTask()
    {
        $taskList = $this->launcher->getTasks();
        foreach($taskList as $task)
            $this->assertInstanceOf(ITask::class,$task);
    }


}
