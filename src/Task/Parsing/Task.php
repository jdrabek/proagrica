<?php
namespace Task\Parsing;

use Task\ITask;

class Task implements ITask
{


    public function launch(): void
    {
        $content = file_get_contents('Resources/script.sh');
        $match=[];
        preg_match('/CLASSPATH=(.*)/',$content,$match);
        echo $match[1]."\n";
    }
}