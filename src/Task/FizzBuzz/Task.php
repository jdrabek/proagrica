<?php
namespace Task\FizzBuzz;

use Task\ITask;

class Task implements ITask
{


    public function launch(): void
    {
        for($i = 0; $i<=100;$i++){
            if($i % 3 == 0 || $i % 5 == 0) {
                if ($i % 3 === 0) {
                    echo "Fizz";
                }
                if ($i % 5 === 0) {
                    echo "Buzz";
                }
            } else {
                echo $i;
            }
            echo "\n";
        }
    }
}