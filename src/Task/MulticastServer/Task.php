<?php

namespace Task\MulticastServer;


use Task\ITask;

class Task implements ITask
{
    public function launch() : void
    {

        $socket = socket_create(AF_INET, SOCK_DGRAM, SOL_UDP);
        socket_bind($socket, '127.0.0.1', 1234);

        $from = '';
        $port = 0;
        socket_recvfrom($socket, $buf, 12, 0, $from, $port);

        echo "Received $buf from remote address $from and remote port $port" . PHP_EOL;

    }

}