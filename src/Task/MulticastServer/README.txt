Write 2 classes (may be more than 2 if you wish)

The specification for the 2 classes is, 
1) A listener that listens on a multicast socket, and prints out what it
receives

2) A Sender that sends "Hello World!" on a multicast socket.

Suggested names are
multicastSender.php
multicastListener.php

Provide guide how to call it

