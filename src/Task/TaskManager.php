<?php
namespace Task;

class TaskManager
{
    const TASKS = [
        'Game',
        'Parsing',
        'FizzBuzz',
        'MulticastServer',
        'MulticastClient'
    ];


    public function getTasks() : iterable {
        foreach (self::TASKS as $task) {
            yield $this->launchTask($task);
        }
    }

    public function launchTask(string $taskName) : void {
        $taskClass = '\Task\\'. $taskName .'\Task';
        (new $taskClass)->launch();
    }

}