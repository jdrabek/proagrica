<?php


namespace Task\Game;


interface IGameConst
{
    const ROCK = 1;
    const PAPER = 2;
    const SCISSORS = 3;
    const LIZARD = 4;
    const SPOCK = 5;

    const MATCHES = array(
        self::ROCK => array(self::SCISSORS, self::LIZARD), // w tablicy przedmioty z ktorymi kamien wygrywa
        self::PAPER => array(self::ROCK, self::SPOCK),
        self::SCISSORS => array(self::PAPER, self::LIZARD),
        self::LIZARD => array(self::SPOCK, self::PAPER),
        self::SPOCK => array(self::SCISSORS, self::ROCK),
    );

    const  NAMES = array(
        "rock"=> self::ROCK,
        "paper"=> self::PAPER,
        "scissors"=> self::SCISSORS,
        "lizard"=> self::LIZARD,
        "spock"=>self::SPOCK,
    );

}