<?php
namespace Task\Game;

use Task\ITask;

class Task implements ITask, IGameConst
{


    public  function play(int $player1, int $player2) : void {
        echo "Przeciwnik wylosował ".array_keys(self::NAMES)[$player2-1]."\n\n";
        if($player2 == $player1) {
            echo "Remis\n";
            return;
        }

        $result = in_array($player2, self::MATCHES[$player1]);

        echo $result ? "player1 wygrywa\n" : "player2 wygrywa\n";

    }

    public function launch() : void
    {
        $userInput = $this->promptForInput();
        if(!$this->checkInput($userInput))
            return;

        $player1 = self::NAMES[$userInput];
        $player2 = rand(1,5);
        $this->play($player1,$player2);

    }

    private function promptForInput() : string
    {
        echo "=== ROCK PAPER SCISSORS LIZARD SPOCK ===\n";
        return readline(
            "Wybierz jeden z przedmiotow ponizej:\n".
            implode('  ',array_keys(self::NAMES))."\n"
        );
    }


    private function checkInput(string $userInput) : bool
    {
        if(!isset(self::NAMES[$userInput])){
            return false;
        }


        if(self::NAMES[$userInput] <1 || self::NAMES[$userInput] > 5){
            echo "niepoprawny argument";
            return false;
        }

        return true;
    }
}