<?php


namespace Task;


interface ITask
{
    public function launch() : void;

}