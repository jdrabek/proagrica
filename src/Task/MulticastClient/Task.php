<?php
namespace Task\MulticastClient;

class Task
{
    public function launch() : void
    {
        $sock = socket_create(AF_INET, SOCK_DGRAM, SOL_UDP);

        $msg = "Hello world!";
        $len = strlen($msg);

        socket_sendto($sock, $msg, $len, 0, '127.0.0.1', 1234);
        socket_close($sock);
        
    }

}